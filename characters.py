import fnmatch
import random
import json
import discord
import io
from discord.ext import commands
from typing import Literal
from collections.abc import Callable

gender_type = Literal['m', 'f', 'unisex', 'any']
distribution_type = Literal['real', 'common', 'rarer',
                            'equal', 'unique']


class Characters(commands.Cog):
    '''Generate random characters for PD and various piecemeal parts (names, schools, archetypes, etc)
    '''

    def __init__(self):
        self.gns = []
        self.gn_set = set()
        self.two_g_set = set()

        self.sns = []

        mn = 0
        fn = 0

        with open("givennames.csv") as gnf:
            # Discard header line
            next(gnf)
            for line in gnf:
                vs = line.split(',')
                if len(vs) != 3:
                    continue

                # (Name, Count, Gender)
                self.gns.append((vs[0], int(vs[2]), vs[1],))
                if vs[1] == 'M':
                    mn += 1
                if vs[1] == 'F':
                    fn += 1
                if vs[0] in self.gn_set:
                    # Already seen, must be a different gender
                    self.two_g_set.add(vs[0])
                else:
                    self.gn_set.add(vs[0])

        with open("surnames.csv") as snf:
            # Discard header line
            next(snf)
            for line in snf:
                vs = line.split(',')
                if len(vs) < 3:
                    continue

                # (Name, Count)
                self.sns.append((vs[0], int(vs[2]),))

        with open('pd.json') as sf:
            data = json.load(sf)
            self.personalities = data['personalities']
            self.goals = data['goals']
            self.archetypes = data['archetypes']
            self.schools = data['schools']

    def filter_pattern(self, nl: list[tuple], pattern: str) -> list[tuple]:
        return filter(lambda nt: fnmatch.fnmatch(nt[0].lower(), pattern.lower()), nl)

    # given names only
    def filter_gender(self, nl: list[tuple], gender: gender_type) -> list[tuple]:
        if gender in ('m', 'f'):
            return filter(lambda nt: nt[2].lower() == gender, nl)
        if gender == 'unisex':
            return filter(lambda nt: nt[0] in self.two_g_set, nl)

        # gender == 'any'
        return nl

    def transform_weights(self, nl: list[tuple], distribution: distribution_type) -> list[tuple]:
        if distribution == 'common':
            return map(lambda nt: (nt[0], int(nt[1] ** 2)), nl)
        if distribution == 'rarer':
            return map(lambda nt: (nt[0], int(nt[1] ** 0.5)), nl)
        if distribution == 'equal':
            return map(lambda nt: (nt[0], 1), nl)
        if distribution == 'unique':
            max_count = 0
            for nt in nl:
                if nt[1] > max_count:
                    max_count = nt[1]
            return map(lambda nt: (nt[0], max_count+100 - nt[1]), nl)

        # distribution == 'real'
        return nl

    def weighted_random_name(self, nl: list[tuple], distribution: distribution_type) -> str:
        reweighted = list(self.transform_weights(nl, distribution))

        total_weight = 0
        for nt in reweighted:
            total_weight += nt[1]

        index = random.randint(0, total_weight)

        for nt in reweighted:
            index -= nt[1]
            if index <= 0:
                return nt[0]

        return 'No name found matching the criteria'

    def get_givenname(self, gender: gender_type = 'any', pattern: str = '', distribution: distribution_type = 'real'):
        if len(pattern):
            patterned_gns = self.filter_pattern(self.gns, pattern)
        else:
            patterned_gns = self.gns

        gendered_gns = self.filter_gender(patterned_gns, gender)

        return self.weighted_random_name(list(gendered_gns), distribution)

    def get_surname(self, pattern: str = '', distribution: distribution_type = 'real'):
        if len(pattern):
            patterned_sns = self.filter_pattern(self.sns, pattern)
        else:
            patterned_sns = self.sns

        return self.weighted_random_name(list(patterned_sns), distribution)

    def get_personality(self):
        type_num = random.randint(0, 3)
        color_num = random.randint(0, 6)
        type = list(self.personalities.keys())[type_num]
        color = list(self.personalities[type].keys())[color_num]
        title = self.personalities[type][color]

        return type, color, title, type_num, color_num

    def get_goal(self):
        output = ''
        goal = random.choice(self.goals)

        if goal[0] == 'XIV':
            output += 'XIV: Once sought ' + \
                random.choice(self.goals[1:])[
                    1] + ', now seeks ' + random.choice(self.goals[1:])[1]
        else:
            output += goal[0] + ': Seeks ' + goal[1]

        return output

    def get_archetype(self):
        roll = random.randint(0, 48)
        diff = random.randint(0, 1)

        return self.archetypes['types'][roll] + self.archetypes['malus'][roll][diff]

    def get_school(self, unrecorded: bool = False, verbose: bool = False, weighted: bool = False):
        field = ''
        subfield = ''
        row = None
        column = None
        subfield_description = ''
        verbose = ''

        loop = True
        while loop:
            if weighted:
                field_roll_1 = random.randint(0, 23)
                field_roll_2 = random.randint(0, 23)
                field_roll = min(field_roll_1, field_roll_2)
                field = list(self.schools['fields'].keys())[field_roll]
                row = random.randint(0, 7)
                if row == 7:
                    if r := self.schools['fields'][field]['rows']:
                        row = random.choice(r)
                    else:
                        row = random.randint(0, 6)
                column = random.randint(0, 5)
                if column == 5:
                    if c := self.schools['fields'][field]['columns']:
                        column = random.choice(c)
                    else:
                        column = random.randint(0, 4)
            else:
                field = random.choice(list(self.schools['fields'].keys()))
                row = random.randint(0, 6)
                column = random.randint(0, 4)
            subfield = self.schools['fields'][field]['subfields'][row][column]
            if isinstance(subfield, list):
                s = random.randint(0, len(subfield) - 1)
                subfield = subfield[s]
            if unrecorded or subfield:
                loop = False

        row = self.schools['rows'][row]
        column = self.schools['columns'][column]
        if subfield in self.schools['subfields-desc'].keys():
            subfield_description = self.schools['subfields-desc'][subfield]
        else:
            subfield_description = "This subfield lacks a description."

        if verbose and (subfield not in self.schools['subfields-desc'].keys() or subfield == ""):
            verbose = "In other fields, this intersection includes:"
            tracker = []
            fs = list(self.schools['fields'].keys())
            fs.remove(field)
            random.shuffle(fs)
            for f in fs:
                sub = self.schools['fields'][f]['subfields'][row][column]
                if isinstance(sub, list):
                    s = random.randint(0, len(sub) - 1)
                    sub = sub[s]
                if sub and sub not in tracker:
                    verbose += "\n[" + f + "] **" + sub + "**"
                    tracker.append(sub)
            verbose += "\n\nThe components of this practice include:\n"
            verbose += "*" + self.schools['rows'][row] + \
                ":* " + self.schools['rows-desc'][row] + "\n"
            verbose += "*" + self.schools['columns'][column] + \
                ":* " + self.schools['columns-desc'][column]

        return field, subfield, column, row, subfield_description, verbose

    g_param = commands.parameter(
        default='any', description='The gender of the name')

    d_param = commands.parameter(
        default='real', description='Skew name rarity - real, common, rarer, equal, or unique')

    p_param = commands.parameter(
        default='', description='Pattern to match - use ? for any one letter, or * to match any number of letters')

    @commands.hybrid_command()
    async def givenname(self, ctx: commands.Context, gender: gender_type = g_param, distribution: distribution_type = d_param, pattern: str = p_param):
        '''Generate a random given name. Can specify gender, distribution, and/or a letter pattern to match.
        Uses US name data. See also /name & /surname
        Example uses:
        Generate a random female name, using the real (observed) distribution: /givenname f
        Generate a random name of any gender, skewing towards common names: /givenname any common
        Generate a random male name, choosing between common and rare names equally: /givenname m equal
        Generate a random name that is used for both men and women (the dataset subscribes to the gender binary, sorry): /givenname unisex
        Generate a random name, skewing rarer, that begins with an A: /givenname any rarer A*
        Generate a random name of the real (observed) distribution that has N as the second letter and ends in D: /givenname any real ?n*d
        '''

        await ctx.send(self.get_givenname(gender, pattern, distribution).capitalize())

    @ commands.hybrid_command()
    async def surname(self, ctx: commands.Context, distribution: distribution_type = d_param, pattern: str = p_param,):
        '''Generate a random surname. Can specify distribution and/or a letter pattern to match.
        Uses US name data. See also /name & /givenname
        Example uses:
        Generate a random surname, using the real (observed) distribution: /surname
        Generate a random name of any gender, skewing towards rarer names: /surname rarer
        Generate a random name that ends in 'son', inverting the weights so rarer names are more common: /surname unique *son
        '''

        await ctx.send(self.get_surname(pattern, distribution).capitalize())

    @ commands.hybrid_command()
    async def name(self, ctx: commands.Context, gender: gender_type = g_param, distribution: distribution_type = d_param, givenname_pattern: str = p_param, surname_pattern: str = p_param):
        '''Generate a random given and surname (first & last). Can specify many options
        Options are gender, distribution, and/or a letter pattern to match for first and last names.
        Uses US name data. See also /givenname & /surname
        Example uses:
        Generate a random female name, using the real (observed) distribution: /name f
        Generate a random name of any gender, skewing towards common names: /name any common
        Generate a random male name, choosing between common and rare names equally: /name m equal
        Generate a random name where the first name is used for both men and women (the dataset subscribes to the gender binary, sorry): /name unisex
        Generate a random male name, skewing rarer, where the given name begins with an S and the second to last letter is an A: /name m rarer S*a?
        Generate a random name of the real (observed) distribution where both names start with P: /name any real P* P*
        Generate a random female name, skewing common, where the last name ends in 'ez': /name f common * *ez
        '''

        name1 = self.get_givenname(
            gender, givenname_pattern, distribution).capitalize()
        name2 = self.get_surname(surname_pattern, distribution).capitalize()

        if name1 == 'No name found matching the criteria':
            await ctx.send('No given name found matching the criteria')
            return

        if name2 == 'No name found matching the criteria':
            await ctx.send('No surname found matching the criteria')
            return

        await ctx.send(f'{name1} {name2}')

    @commands.hybrid_command()
    async def personality(self, ctx):
        '''Generate a random personality for Pact Dice.
        '''
        type, color, title, _, _ = self.get_personality()

        await ctx.send(f'{title}\n*{type} x {color}*')

    @commands.hybrid_command()
    async def goal(self, ctx):
        '''Generate a random goal for Pact Dice.
        '''
        goal = self.get_goal()

        await ctx.send(goal)

    @commands.hybrid_command()
    async def archetypes(self, ctx):
        '''Generate three random archetypes for Pact Dice.
        '''
        output = ''
        for x in range(3):
            output += self.get_archetype() + '\n'

        await ctx.send(output[:-1])

    @commands.hybrid_command()
    async def archetype(self, ctx, amount: int = 1):
        '''Generate one or more random archetypes for Pact Dice.
        '''
        output = ''
        for x in range(amount):
            output += self.get_archetype() + '\n'

        await ctx.send(output[:-1])

    u_param = commands.parameter(
        default=False,
        description="Include unrecorded intersections: field, column, and row combinations that don't even have a name associated with them. For if you want to come up with those yourself.")
    v_param = commands.parameter(
        default=False,
        description="Certain results become wordier. If the generator selects a subfield without a description or an unrecorded intersection, the names of subfields in that intersection in other fields and descriptions of the column and row components will be provided.")
    w_param = commands.parameter(
        default=False,
        description="Use weighted rolls. Rolls for field twice and takes the result higher on the list.")

    @commands.hybrid_command()
    async def school(self, ctx, unrecorded: bool = u_param, verbose: bool = v_param, weighted: bool = w_param):
        '''Generates random school for Pact Dice.
        May include arguments for unrecorded, verbose, and weighted rolls.'''

        output = ""

        field, subfield, column, row, subfield_description, verbose = self.get_school(
            unrecorded, verbose, weighted)

        output += "[" + field + "]\n"
        if subfield:
            output += "**" + subfield + "**\n"
        output += "*" + column + " x " + row + "*" + "\n"
        output += subfield_description + "\n"
        output += verbose

        await ctx.send(output)

    @commands.hybrid_command()
    async def npc(self, ctx):
        '''Generates an NPC (name, personality, archetype).
        '''

        name1 = self.get_givenname().capitalize()
        name2 = self.get_surname().capitalize()

        type, color, title, _, _ = self.get_personality()

        archetype = self.get_archetype()

        output = f'**{name1} {name2}**\n{title} ({type} x {color})\n{archetype}'

        await ctx.send(output)

    @commands.hybrid_command()
    async def practitioner(self, ctx):
        '''Generates a practitioner (name, personality, archetype, school, goal).
        '''

        name1 = self.get_givenname().capitalize()
        name2 = self.get_surname().capitalize()

        type, color, title, _, _ = self.get_personality()

        archetype = self.get_archetype()

        field, subfield, column, row, _, _ = self.get_school()

        goal = self.get_goal()

        output = f'**{name1} {name2}**\n{title} ({type} x {color})\n{archetype}\n{subfield} ({field}, {column} x {row})\n{goal}'

        await ctx.send(output)

    @commands.hybrid_command()
    async def family(self, ctx):
        '''Generates a practitioner family.
        '''
        output = ''

        top = random.randint(1, 4)

        surname = self.get_surname().capitalize()
        field, subfield, column, row, _, _ = self.get_school()
        type, color, title, type_num, color_num = self.get_personality()

        self.count = 1

        def make_relative(generation: int, primary: bool, spacer: str, branch_spacer: str):
            final_sib = '└' in branch_spacer
            if final_sib:
                output = f'\n{spacer[:-5] + "│"}\n{branch_spacer}'
            else:
                output = f'\n{spacer}\n{branch_spacer}'

            gender = 0
            r = random.random()
            if r < 0.45:
                gender = 1
            elif r > 0.9:
                gender = 2
            primary_name = ''
            if gender == 0:
                primary_name = self.get_givenname(
                    'm', distribution='common')
            elif gender == 1:
                primary_name = self.get_givenname(
                    'f', distribution='common')
            elif gender == 2:
                primary_name = self.get_givenname(
                    'unisex', distribution='common')

            labels: str = (
                (('Patriarch', 'Matriarch', 'Family Head'),
                 ('Heir', 'Heiress', 'Heir'),
                 ('Grandson', 'Granddaughter', 'Grandchild')) if primary else
                (('Uncle', 'Aunt', 'Family Head\'s Sibling'),
                 ('Son', 'Daughter', 'Child'),
                 ('Grandson', 'Granddaughter', 'Grandchild'))
            )[generation]

            output += f'{primary_name} ({labels[gender]})'

            output += member_stats(spacer)

            spouse_odds: float = (0.5, 0.3, 0)[generation]
            spouse = random.random() < spouse_odds
            if spouse:
                output += f'\n{spacer}│\n{spacer}'
                if gender == 0:
                    g = 'f' if random.random() < 0.9 else 'm'
                    spouse_name = self.get_givenname(
                        g, distribution='common')
                elif gender == 1:
                    g = 'm' if random.random() < 0.9 else 'f'
                    spouse_name = self.get_givenname(
                        g, distribution='common')
                elif gender == 2:
                    spouse_name = self.get_givenname(
                        'any', distribution='common')
                output += f'{spouse_name} ({primary_name}\'s Partner)'

                output += member_stats(spacer)

            child_count: int = 0

            if generation == 0:
                child_count = 1 + \
                    random.choice([0, 1, 2, 3]) if primary else random.choice(
                        [0, 1, 2, 3]
                    )
            elif generation == 1:
                child_count = random.choice([0, 0, 0, 1, 1, 2])

            child_spacer = spacer + '│    '
            child_branch_spacer = spacer + '├────'
            final_child_spacer = spacer + '     '
            final_child_branch_spacer = spacer + '└────'

            if child_count > 1:
                output += make_relative(generation + 1,
                                        primary, child_spacer, child_branch_spacer)
                child_count -= 1
            elif child_count == 1:
                output += make_relative(generation + 1,
                                        primary, final_child_spacer, final_child_branch_spacer)
                child_count -= 1

            while child_count > 1:
                output += make_relative(generation + 1,
                                        False, child_spacer, child_branch_spacer)
                child_count -= 1

            if child_count > 0:
                output += make_relative(generation + 1,
                                        False, final_child_spacer, final_child_branch_spacer)

            return output

        def member_stats(spacer: str):
            t = random.randint(1, 6)
            c = random.randint(1, 6)
            if t < 4:
                t = type
            elif t == 4:
                t = list(self.personalities.keys())[type_num - 1]
            elif t == 5:
                t = list(self.personalities.keys())[(type_num + 1) % 4]
            elif t == 6:
                t = list(self.personalities.keys())[type_num - 2]
            if c < 4:
                c = color
            elif c == 4:
                c = list(self.personalities[t].keys())[color_num - 1]
            elif c == 5:
                c = list(self.personalities[t].keys())[(color_num + 1) % 6]
            elif c == 6:
                c = list(self.personalities[t].keys())[
                    color_num - random.randint(3, 4)]
            g = self.get_goal()
            return f'\n{spacer}{self.personalities[t][c]} ({t} x {c})\n{spacer}{g}'

        output += f'The {surname} Family\n{title} ({type} x {color})\n{field} ({subfield}, {column} x {row})\n'

        output += make_relative(0, True, '', '')
        for _ in range(top):
            output += make_relative(0, False, '', '')

        f = io.BytesIO(output.replace('*', '').encode())
        await ctx.send(file=discord.File(f, 'family.txt'))
