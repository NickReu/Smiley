from discord.ext import commands
import random


class Snacks(commands.Cog):
    '''Add snacks to the pile, or eat 'em up.'''

    def __init__(self):
        self.snacks = {}
        self.contributors = {}
        self.snackers = {}
        self.eatings = [
            'chows down on some {}.',
            'gorges themself on {}!',
            'can\'t get enough {}!',
            'chomps on {}.',
            'guzzles down some {}.',
            'delicately partakes in some {}.',
            'loudly masticates {}.',
            'eats a whole {}.',
            'eats some {}.',
            'nibbles some {}.',
            'devours some {}.',
            'eviscerates the {}!',
            'dumps {} into their large mouth.',
            'slides some {} right down their gullet.'
        ]
        self.offers = {}

    @commands.hybrid_group()
    async def snack(self, ctx: commands.Context) -> None:
        '''Add snacks to the pile, or eat 'em up.'''
        pass

    @snack.command()
    async def eat(self, ctx: commands.Context, snack: str = '') -> None:
        '''Gobble a snack!'''
        if len(self.snacks) == 0:
            await ctx.send('Sorry, all the snacks are gone!')
            return

        if snack == '':
            snack = random.choice(list(self.snacks.keys()))

        if snack not in self.snacks:
            await ctx.send(f'{snack} doesn\'t seem to be in the snack pile')
        else:
            if self.snacks[snack] == 1:
                del self.snacks[snack]
            else:
                self.snacks[snack] -= 1
            await ctx.send(ctx.author.name + ' ' + random.choice(self.eatings).format(snack))

    @snack.command()
    async def add(self, ctx: commands.Context, snack: str, amount: int = 1) -> None:
        '''Add one or more snacks to the pile.'''
        if snack not in self.snacks:
            self.snacks[snack] = 0
        self.snacks[snack] += amount
        await ctx.send('Added!')

    @snack.command()
    async def search(self, ctx: commands.Context) -> None:
        '''Inspect the pile of snacks.'''
        if len(self.snacks) == 0:
            await ctx.send('There are no snacks in the pile!')
            return
        snacks = ', '.join(
            [f'{self.snacks[s]} {s}' for s in self.snacks.keys()])
        await ctx.send(f'In the pile there are:\n> {snacks}')
