from typing import Literal
from discord.ext import commands
import discord
import sheets
import random


async def longsend(ctx, content):
    if len(content) > 1990:
        msgs = content.split('\n')
    else:
        msgs = [content]
    found_too_long = True
    while found_too_long:
        found_too_long = False
        for i, msg in enumerate(msgs):
            if len(msg) > 1990:
                found_too_long = True
                msgs[i] = msg[:1990]
                msgs.insert(i+1, msg[1990:])
    for msg in msgs:
        await ctx.send(msg)


class Triggers_And_More(commands.Cog):
    @commands.hybrid_command()
    async def trigger(self, ctx, index: int = 0):
        '''Get a trigger for a character. Can specify index starting from 1.
        '''
        t = (await sheets.trigger(index))

        if str(t) != "":
            await longsend(ctx, t)
            await ctx.author.send("You have rolled a trigger, presumably for a " +
                                  "game. Make sure at the end of the gen that your GM performs `%claim` " +
                                  "and `%increment`. Have them make sure the trigger remains in the " +
                                  "same place using `%trigger {}`".format(t.split(":")[0]))
        else:
            await ctx.send("Could not find a trigger at " + str(index))

    @commands.hybrid_command()
    async def used(self, ctx, index: int = 0):
        '''Get a used trigger. Can specify index starting from 1.
        '''
        t = (await sheets.used(index))

        if str(t) != "":
            await longsend(ctx, t)
        else:
            await ctx.send("Could not find a used trigger at " + str(index))

    @commands.hybrid_command()
    async def claim(self, ctx, trigger_index: int, game_name: str, player_name: str, power_description: str):
        '''Moves a trigger from unrolled triggers to used triggers
        Format for claim: `%claim <number> <game> <player> <short description of power>`
        It's important to put quotes around any arguments that are more than one word, if not using the slash command!
        Ex: %claim 32 "New York" Greg "Blaster who launches hungry ghost sharks"'''
        res = await sheets.claim(trigger_index, game_name, player_name, power_description)
        await ctx.send(res)

    @commands.hybrid_command()
    async def increment(self, ctx, category: Literal[
        'mover', 'shaker', 'brute', 'breaker', 'master', 'tinker', 'blaster', 'thinker', 'striker', 'changer', 'trump', 'stranger'
    ], amount: float):
        '''Increments a category amount on the trigger sheet.
        Can do decimal values, if, for example, your newly genned cape is part blaster part mover
        %increment mover 0.3
        %increment blaster 0.7
        Single cape's categories should sum to 1
        '''

        success = (await sheets.increment(category, amount))
        if success:
            await ctx.send("Incremented {} by {}".format(category, amount))
        else:
            await ctx.send("Failed to increment", ephemeral=True)

    @commands.hybrid_group()
    async def luck(self, ctx):
        '''Get randomised luck for WD/PD. Can also search by passing the title.'''
        pass

    @luck.command()
    async def search(self, ctx, query: str, include_fan: bool = True):
        '''Search for a specific luck result.'''
        for col in range(1, 5):
            output = await sheets.luck(col, include_fan, query)
            if output:
                if (col == 1):
                    await ctx.send("*Life Perk*: " + output)
                elif (col == 2):
                    await ctx.send("*Power Perk*: " + output)
                elif (col == 3):
                    await ctx.send("*Life Flaw*: " + output)
                elif (col == 4):
                    await ctx.send("*Power Flaw*: " + output)
                return
        await ctx.send(f'Couldn\'t find a perk or flaw matching \'{query}\'', ephemeral=True)

    @luck.command()
    async def draw(self, ctx, include_fan: bool = True):
        '''Draw two random perks or flaws for character creation.'''
        for _ in range(2):
            choice = random.randint(1, 4)
            # Ordering for these is Perk Life, Perk Power, Flaw Life, Flaw Power
            output = (await sheets.luck(choice, include_fan))
            if (choice == 1):
                await ctx.send("*Life Perk*: " + output)
            elif (choice == 2):
                await ctx.send("*Power Perk*: " + output)
            elif (choice == 3):
                await ctx.send("*Life Flaw*: " + output)
            elif (choice == 4):
                await ctx.send("*Power Flaw*: " + output)

    @luck.command()
    async def get(self, ctx, result_type: Literal['perk', 'flaw', 'power', 'life', 'power perk', 'life perk', 'power flaw', 'life flaw', 'random'], include_fan: bool = True):
        '''Draw one random luck result.'''
        cols = {
            'perk': (1, 2),
            'flaw': (3, 4),
            'power': (2, 4),
            'life': (1, 3),
            'life perk': (1,),
            'power perk': (2,),
            'life flaw': (3,),
            'power flaw': (4,),
            'random': (1, 2, 3, 4)
        }

        choice = random.choice(cols[result_type])
        # Ordering for these is Perk Life, Perk Power, Flaw Life, Flaw Power
        output = (await sheets.luck(choice, include_fan))
        if (choice == 1):
            await ctx.send("*Life Perk*: " + output)
        elif (choice == 2):
            await ctx.send("*Power Perk*: " + output)
        elif (choice == 3):
            await ctx.send("*Life Flaw*: " + output)
        elif (choice == 4):
            await ctx.send("*Power Flaw*: " + output)

    @commands.hybrid_command()
    async def status2(self, ctx, *, effect: Literal['bleed', 'scar', 'blinded', 'deafened', 'disabled', 'disarmed', 'knocked down', 'staggered', 'confused', 'pain', 'death sentence']):
        '''Return info on a specified status effect from old WD rules.

           The single argument is the name of a status effect.
        '''
        status_d = {'bleed': "Bleed - If the subject does not take a full round to patch themselves up, they suffer a minor wound after [Guts score] turns.",
                    'scar': "Scar - The injury looks bad, it’s hard to hide, and it takes twice as long to recover from, whether through rest/over time or by way of medical attention.",
                    'blinded': "Blinded - Similar to Disabled. Must roll Wits for even mundane attempts to see surroundings (those that would not have to be rolled for). Make roll at -2 penalty, typically a 4+ to succeed; attempts at evasion or other factors may make this more difficult. Otherwise limited to sensing things within 5’. Deafened is the same thing, but for hearing/communication.",
                    'deafened': "Blinded - Similar to Disabled. Must roll Wits for even mundane attempts to hear surroundings (those that would not have to be rolled for). Make roll at -2 penalty, typically a 4+ to succeed; attempts at evasion or other factors may make this more difficult. Otherwise limited to sensing things within 5’. Blinded is the same thing, but for sight.",
                    'disabled': "Disabled - Typically affects a limb. Must roll [appropriate stat] for even mundane attempts to use limb (those that wound not have to be rolled for). Make roll at -2 penalty, typically a 4+ to succeed. Can use arms for very light (5 lbs or less) burdens, can move at walking pace.",
                    'disarmed': "Disarmed - Held item is knocked into a space within 10’. 5’ for items weighing 5 lbs or more.",
                    'knocked down': "Knocked Down - Ass hits the floor. Standing or attempting a close-quarters attack and failing provokes an attack from those nearby, movement allowance is reduced by half in the process of getting up.",
                    'staggered': "Staggered - Off balance, shoved away. Get moved back a distance, typically 5’. Adjust by 5’ one way or the other depending on difference in Brawn, size; heavier people and armored individuals move less. Staggered individuals are penalized on their next turn: attacks suffer -1 and their movement is reduced by ½ if they try to move in a direction they weren’t pushed in, by ¾ if they try to move against that direction.",
                    'confused': "Confused - Concussed, thoughts scattered. Wits roll (4+) to identify targets, directions, where things are respective to each other. On a failure, can still act, but targets are chosen randomly, movement runs risk of stumbling into wall.",
                    'pain': "Pain - Suffer a temporary minor wound when exerting the affected part, or when the part is struck again, with the wound fading at the end of the next turn, if another wasn’t inflicted. ‘Exertion’ is respective to body part - arm is limited in tests of strength, rare Athletics checks (ie. climbing), leg is movements faster than a walk, jumping, climbing, body is making any Guts or Athletic checks for stamina, and head is any Wits or Know check (typically only a psychic attack).",
                    'death sentence': "Death Sentence - Subject is dying. Each round, an empty wound slot fills up with a moderate wound. Once filled, moderate wounds start becoming critical ones. Typically helpless, intervention can stop or slow progression."}
        effect = effect.lower().strip()
        if effect not in status_d:
            await ctx.send("I don't know that status effect", ephemeral=True)
            return
        else:
            await ctx.send(status_d[effect])

    @commands.hybrid_command()
    async def status3(self, ctx, *, effect: Literal['bleeding', 'pained', 'scarring', 'blinded', 'deafened', 'knocked down', 'delayed', 'stuck', 'death sentence']):
        '''Return info on a specified status effect from new WD rules.

           The single argument is the name of a status effect.
        '''
        status_d = {'bleeding': 'Suffer a lesser wound when expending effort or taking a full action.  Resting mid-combat prevents the next instance and can allow a roll to remove bleed.  Full rests remove bleed without needing a roll.  When the combat encounter ends, if bleeding wasn’t removed, then bleeding ends and leaves a lesser wound in its place.',
                    'pained': '''(Specific to stats, see subheadings for examples, special cases)
Normal actions that wouldn’t normally require a roll now do; on a failure they have half effect where applicable (die rolls, distance).
For actions that would require a roll, on a failure the subject must choose either a decline in morale, a lesser wound, or end their turn prematurely (this option is available only if their turn is available to lose).
- Physical Pain: Applies to any exertion, encompassing Brawn, Ath, Dex, & Guts
- Mental Pain: Applies to Wits, personal/defensive Social checks, Know.
- Emotional Pain: Social pain only applies for personal & defensive rolls.
- Pain in (body part): Applies when using that body part for an action''',
                    'scarring': 'Pairs with a wound and/or status effect to indicate that wound is not easily healed.  Scarring wounds & effects persist until specific end-of-day downtime can be dedicated to taking care of them or medical care given.  Each requires individual attention.',
                    'blinded': 'Imposes an inability to see past 10’ (doubled for large or obvious targets) and a -1 to hit those that can be seen.',
                    'deafened': 'Imposes an inability to hear past 10’ (doubled for loud or obvious targets).',
                    'knocked down': 'The fallen individual can’t rise without a movement action (taking up half the movement).  Rising and taking any combat action that isn’t offensive or defensive against melee attackers provokes melee attacks.  Can’t move more than 5’ while knocked down.',
                    'delayed': 'Delays the target’s next turn, adjusting the list of who gets to act when to put the delayed individual after the enemy or group of enemies who would otherwise act next.',
                    'stuck': '''The weapon, piercing object, or projectile is caught inside the target.  If it is removed forcefully or carelessly (including forcibly with a wrest in a grapple), the target suffers a moderate wound and bleeding.  A lengthy (Dexterity) check of 12 reduces this to lesser.
Wounds with objects stuck in them cannot fully heal.
- Stuck (From Melee): The weapon or piercing object is caught inside the target, allows the attacker to segue straight into a grapple with a +1 each round.  The stuck individual can’t exit the grapple without forcefully removing the object.  The attacker can wrest it free as well, in which case they get a free attack on the target.
- Stuck (From Ranged): The projectile is caught inside the target.  If the line of fire and surroundings are appropriate, the target is pinned to the wall and cannot move from the spot until they forcefully extract the object from themselves, make a Lengthy DC check of 6 (Brawn) to free themselves with the object still in them, or extract it carefully as noted above.''',
                    'death sentence': 'The target suffers a lesser wound every round; when full, lesser wounds escalate to moderate one by one.  They cannot recover from the dying state or Death Sentence without outside assistance or medical help.',
                    }
        effect = effect.lower().strip()
        if effect not in status_d:
            effects = ", ".join([k for k in status_d.keys()])
            await ctx.send(f'I don\'t know that status effect. Possible effects are {effects}', ephemeral=True)
            return
        else:
            await ctx.send(status_d[effect])
