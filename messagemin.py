from discord.ext import commands
import discord


class MessageMin(commands.Cog):

    def __init__(self, b):
        self.b = b
        self.lims = {}

    @commands.Cog.listener()
    async def on_message(self, message):
        if message.channel.id in self.lims:
            if (not message.author.bot) and 'Mod Team' not in (str(role) for role in message.author.roles) and 'Author' not in (str(role) for role in message.author.roles):
                if self.lims[message.channel.id] == -1:
                    await message.delete()
                    try:
                        await message.author.send("Sorry, #" + str(message.channel.name) + " is silenced. Your message: ```" + str(message.content) + "``` has been deleted.")
                    except discord.errors.Forbidden:
                        return
                elif len(message.content) < self.lims[message.channel.id]:
                    await message.delete()
                    try:
                        await message.author.send("Sorry, #" + str(message.channel.name) + " currently has a minimum character requirement of " + str(self.lims[message.channel.id]) + ". Your message: ```" + str(message.content) + "``` has been deleted.")
                    except discord.errors.Forbidden:
                        return

    @commands.hybrid_command()
    async def setmin(self, ctx, character_minimum: int):
        '''Moderators only. Establish a mimimum character limit for messages in the current channel.
        '''
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and ('Author' not in (str(role) for role in ctx.author.roles)):
            await ctx.send("Only moderators may use this command.")
            return
        self.lims[ctx.message.channel.id] = character_minimum
        if character_minimum == 0:
            del self.lims[ctx.message.channel.id]
            await ctx.send(f'Character minimum removed.')
            return
        await ctx.send(f'Character minimum set to {character_minimum}. Set minimum to 0 to remove limit.')

    @commands.hybrid_command()
    async def silence(self, ctx):
        '''Moderators only. Silences a channel so that no one can post in it. Must be called again to remove.
        '''
        if ('Mod Team' not in (str(role) for role in ctx.author.roles)) and ('Author' not in (str(role) for role in ctx.author.roles)):
            await ctx.send("Only moderators may use this command.")
            return
        if ctx.message.channel.id in list(self.lims.keys()) and self.lims[ctx.message.channel.id] == -1:
            del self.lims[ctx.message.channel.id]
            await ctx.send("Channel unsilenced.")
        else:
            self.lims[ctx.message.channel.id] = -1
            await ctx.send("Channel silenced. A mod may use %silence again to turn it back on.")
