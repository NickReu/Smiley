from discord.ext import commands
from discord import Thread
from typing import Literal
import random

import smiley_emoji
import constants


class Liveread(commands.Cog):
    @commands.hybrid_group()
    async def liveread(self, ctx):
        '''Join or leave the liveread channel.
        Usage: /liveread <enter/exit/rules> <I have read the rules!>
               i.e. /liveread enter I have read the rules!
        '''
        pass

    @liveread.command()
    async def enter(self, ctx, *,
                    have_you_read_the_liveread_rules:
                    Literal['I have read the rules',
                            'Not yet, where can I find these lovely rules?']
                    = 'Not yet, where can I find these lovely rules?'):
        '''Enter the livereads channel.'''
        if have_you_read_the_liveread_rules != 'I have read the rules':
            await ctx.send('Please read the rules of the liveread channel!\nThey can be found here: https://docs.google.com/document/d/1wl_Wl_wk884MNzeBkfqOKgmj8BaGPsVpfou13pReBEg/edit?usp=sharing',
                           ephemeral=True)
            return
        for channel in ctx.guild.channels:
            if channel.name == 'livereads' and channel.category.name.lower() == 'serial discussion':
                await channel.set_permissions(ctx.author, read_messages=True)
                break
        await ctx.send('Entered!', ephemeral=True)

    @liveread.command()
    async def enter_legacy(self, ctx, legacy_channel: Literal['worm', 'ward', 'pact', 'pale', 'twigverseplus', 'all'], *,
                           have_you_read_the_liveread_rules:
                           Literal['I have read the rules',
                                   'Not yet, where can I find these lovely rules?']
                           = 'Not yet, where can I find these lovely rules?'):
        '''Enter an archived livereads channel to read legacy livereads.'''
        if have_you_read_the_liveread_rules != 'I have read the rules':
            await ctx.send('Please read the rules of the liveread channel!\nThey can be found here: https://docs.google.com/document/d/1wl_Wl_wk884MNzeBkfqOKgmj8BaGPsVpfou13pReBEg/edit?usp=sharing',
                           ephemeral=True)
            return
        for channel in ctx.guild.channels:
            if channel.name == f'{legacy_channel}-livereads' or ('livereads' in channel.name and legacy_channel == 'all') and channel.category.name in constants.archive_categories:
                await channel.set_permissions(ctx.author, read_messages=True)
                if legacy_channel != 'all':
                    break
        await ctx.send('Entered!', ephemeral=True)

    @liveread.command()
    async def exit_legacy(self, ctx, legacy_channel: Literal['worm', 'ward', 'pact', 'pale', 'twigverseplus', 'all']):
        '''Exit an archived livereads channel.'''
        for channel in ctx.guild.channels:
            if channel.name == f'{legacy_channel}-livereads' or ('livereads' in channel.name and legacy_channel == 'all') and channel.category.name in constants.archive_categories:
                await channel.set_permissions(ctx.author, read_messages=False)
        await ctx.send('Exited!', ephemeral=True)

    @liveread.command()
    async def exit(self, ctx):
        '''Enter the livereads channel.'''
        for channel in ctx.guild.channels:
            if channel.name == 'livereads' and channel.category.name.lower() == 'serial discussion':
                await channel.set_permissions(ctx.author, read_messages=False)
                break
        await ctx.send('Exited!', ephemeral=True)

    @liveread.command()
    async def rules(self, ctx):
        '''See the livereads channel rules.'''
        await ctx.send("Here you go: https://docs.google.com/document/d/1wl_Wl_wk884MNzeBkfqOKgmj8BaGPsVpfou13pReBEg/edit?usp=sharing")

    @commands.hybrid_command()
    async def buffer(self, ctx, *, spoilers_until: str):
        '''Print a long message to prevent people from seeing spoilers.
        Only works in liveread channels (see %liveread rules).
        Usage: /buffer <Chapter>
            ex. /buffer Ward 5.2
                /buffer Pale One After Another, interlude D'''
        if not ('liveread' in ctx.channel.name or (isinstance(ctx.channel, Thread) and 'liveread' in ctx.channel.parent.name)):
            await ctx.send('Error: I will only buffer liveread channels!', ephemeral=True)
            return
        emoji = smiley_emoji.random_emoji()
        msg = (emoji + '\n') * 60 + spoilers_until
        await ctx.send(msg)
