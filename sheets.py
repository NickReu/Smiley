import pickle
import os.path
import datetime
import random
import difflib
import string
from googleapiclient.discovery import build
from google.oauth2 import service_account as s_a

path_here = os.path.dirname(os.path.realpath(__file__))

SECRET = os.path.join(path_here, "gsecret.json")
SCOPES = ['https://www.googleapis.com/auth/drive']

c = s_a.Credentials.from_service_account_file(SECRET, scopes=SCOPES)

service = build('sheets', 'v4', credentials=c)
drive_service = build('drive', 'v3', credentials=c)


ID1 = '1Foxb_C_zKvLuSMOB4HN5tRMpVwtPrkq6tdlokKSgEqY'
# This is now the permanent trigger doc
TriggerID = '1bWigKxmpEObOWTP0uRA_xwmMnF3Lpk9ZQer5msl6WnA'
TriggerSuggestionsID = '1IZmqUsIXKejHKycZoUEz-agXiQYgoxedz_XF80x8zF4'
DetailID = '1aHyZ7c7TIgt903mPinOakrgli2WZu5IRtiGYPCnCqDE'
SuggestionID = '1kWxWhvKzAYl98nuvgCQOchw7mgH7aecyB82hSwMtatQ'
TradingDatabaseID = '1_1X3GkqQDL9GWkZ9Wm3xw-4Iya-kur8S__Dj0r3FwlE'
CapesDatabaseID = '1_syrsmptzWG0u3xdY3qzutYToY1t3I8s6yaryIpfckU'


async def newgame(name, GM, type):

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Campaigns!A1:E1000').execute()
    values = result.get('values', [])

    rowNum = 0

    name = name.lower()

    for row in values:
        if row[0] != "":
            rowNum = rowNum + 1

    cell = (rowNum, 0)
    celldata = {"userEnteredValue": {"stringValue": name}}
    update_cells = {"rows": [{"values": [celldata]}],
                    "fields": "userEnteredValue",
                    "start": {"sheetId": 0, "rowIndex": cell[0], "columnIndex": cell[1]}}
    requests = [{"updateCells": update_cells}]
    batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                  "requests": requests}).execute()

    cell = (rowNum, 1)
    celldata = {"userEnteredValue": {"stringValue": GM}}
    update_cells = {"rows": [{"values": [celldata]}],
                    "fields": "userEnteredValue",
                    "start": {"sheetId": 0, "rowIndex": cell[0], "columnIndex": cell[1]}}
    requests = [{"updateCells": update_cells}]
    batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                  "requests": requests}).execute()

    cell = (rowNum, 2)
    celldata = {"userEnteredValue": {"stringValue": 'Y'}}
    update_cells = {"rows": [{"values": [celldata]}],
                    "fields": "userEnteredValue",
                    "start": {"sheetId": 0, "rowIndex": cell[0], "columnIndex": cell[1]}}
    requests = [{"updateCells": update_cells}]
    batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                  "requests": requests}).execute()

    cell = (rowNum, 3)
    celldata = {"userEnteredValue": {"stringValue": type}}
    update_cells = {"rows": [{"values": [celldata]}],
                    "fields": "userEnteredValue",
                    "start": {"sheetId": 0, "rowIndex": cell[0], "columnIndex": cell[1]}}
    requests = [{"updateCells": update_cells}]
    batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                  "requests": requests}).execute()


async def category(game):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Campaigns!A1:E1000').execute()
    values = result.get('values', [])

    game = game.lower()

    for row in values:
        if str(row[0]) == str('#' + game):
            return str(row[3])

    return None


async def gamecheck(name, game):

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Campaigns!A1:E1000').execute()
    values = result.get('values', [])

    check = False

    game = game.lower()

    for row in values:
        if str(row[0]) == str('#' + game):
            if str(row[1]) == str(name):
                check = True

    return check

# Find the owner of a specified game


async def ownercheck(game):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Campaigns!A1:E1000').execute()
    values = result.get('values', [])
    game = game.lower()

    for row in values:
        if str(row[0]) == str('#' + game):
            return row[1]

    return ''


async def addlink(name, campaign, link):

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Campaigns!A1:E1000').execute()
    values = result.get('values', [])

    rowNum = 0

    for row in values:
        if str(row[1]) == str(name) and str(row[0]) == ('#'+campaign):
            cell = (rowNum, 4)
            celldata = {"userEnteredValue": {"stringValue": link}}
            update_cells = {"rows": [{"values": [celldata]}],
                            "fields": "userEnteredValue",
                            "start": {"sheetId": 0, "rowIndex": cell[0], "columnIndex": cell[1]}}
            requests = [{"updateCells": update_cells}]
            batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                          "requests": requests}).execute()
            return False
        else:
            rowNum = rowNum + 1
    return True


async def changeState(name, yesno):

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Campaigns!A1:E1000').execute()
    values = result.get('values', [])

    rowNum = 0

    name = name.lower()

    for row in values:
        if str(row[0]) == str('#' + name):
            break
        else:
            rowNum = rowNum + 1

    cell = (rowNum, 2)
    celldata = {"userEnteredValue": {"stringValue": yesno}}
    update_cells = {"rows": [{"values": [celldata]}],
                    "fields": "userEnteredValue",
                    "start": {"sheetId": 0, "rowIndex": cell[0], "columnIndex": cell[1]}}
    requests = [{"updateCells": update_cells}]
    batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                  "requests": requests}).execute()


async def increment(category, value):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerID,
                                range='Count & Frontpage!A7:B18').execute()
    values = result.get('values', [])
    index = 6

    for row in values:
        if str(row[0]).lower() == category:
            current = float(row[1])
            new = current + value
            celldata = {"userEnteredValue": {"numberValue": new}}
            update_cells = {"rows": [{"values": [celldata]}],
                            "fields": "userEnteredValue",
                            "start": {"sheetId": 0, "rowIndex": index, "columnIndex": 1}}
            requests = [{"updateCells": update_cells}]
            batch_res = sheet.batchUpdate(spreadsheetId=TriggerID, body={
                                          "requests": requests}).execute()
            return True
        else:
            index += 1

    return False


async def claim(number, game, player, desc):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerID,
                                range='Triggers!A1:B100').execute()
    values = result.get('values', [])
    num_rows = 0
    for row in values:
        if str(row[0]) != "":
            num_rows += 1
        else:
            break
    index = number
    if index > num_rows or index < 0:
        return "Can't find a trigger at index " + str(index)

    if index == 0:
        index = random.randrange(0, rowNum)
    else:
        # Because the user specifies from 1
        index -= 1

    text = str(values[index][0])
    author = str(values[index][1])
    del_req = {
        "deleteDimension": {
            "range": {
                "sheetId": 1146274495,  # Triggers
                "dimension": "ROWS",
                "startIndex": index,
                "endIndex": index+1
            }
        }
    }

    result = sheet.values().get(spreadsheetId=TriggerID,
                                range='Used!A1:B1000').execute()
    values = result.get('values', [])
    num_used_rows = 1
    for row in values[1:]:
        if str(row[0]) != "" or str(row[1]) != "":
            num_used_rows += 1
        else:
            break

    insert_req = {
        "insertDimension": {
            "range": {
                "sheetId": 239110446,
                "dimension": "ROWS",
                "startIndex": num_used_rows,
                "endIndex": num_used_rows + 1
            },
            "inheritFromBefore": True
        }
    }

    data_to_paste = '<table><tr><td>' + game + \
        '</td><td>' + text + \
        '</td><td>' + author + \
        '</td><td>' + player + \
        '</td><td>' + desc + '</tr></table>'

    paste_req = {
        "pasteData": {
            "coordinate": {
                "sheetId": 239110446,
                "rowIndex": num_used_rows,
                "columnIndex": 0
            },
            "data": data_to_paste,
            "type": "PASTE_VALUES",
            "html": True
        }
    }
    reqs = [del_req, insert_req, paste_req]
    batch_res = sheet.batchUpdate(spreadsheetId=TriggerID, body={
                                  "requests": reqs}).execute()
    return "Claimed trigger " + str(number) + " at used " + str(num_used_rows) + "!"


async def trigger(index):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerID,
                                range='Triggers!A1:B100').execute()
    values = result.get('values', [])

    rowNum = 0
    for row in values:
        if str(row[0]) != "":
            rowNum += 1
        else:
            break

    if index > rowNum or index < 0:
        # Can not find a trigger at this index
        return ''

    if index == 0:
        index = random.randrange(0, rowNum)
    else:
        # Because the user specifies from 1
        index -= 1

    return str(index + 1) + ": " + str(values[index][0])


async def used(index):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerID,
                                range='Used!A1:B1000').execute()
    values = result.get('values', [])

    usedTriggers = []
    # Go through and build
    for row in values[1:]:
        if not row:
            break
        if str(row[1]) != "":
            usedTriggers += [str(row[1])]

    if index > len(usedTriggers) or index < 0:
        # Can not find a trigger at this index
        return ''

    if index == 0:
        index = random.randrange(0, len(usedTriggers))
    else:
        index -= 1

    return str(index + 1) + ": " + str(usedTriggers[index])


async def luck(column, beta=False, search=None):
    sheet = service.spreadsheets()
    index = None
    if not beta:
        result = sheet.values().get(spreadsheetId=DetailID,
                                    range='LUCK!A1:F79').execute()
    else:
        result = sheet.values().get(spreadsheetId=SuggestionID,
                                    range='Perks and Flaws!A1:F79').execute()
    values = result.get('values', [])
    relevantLuck = []
    count = 1
    # Go through specific column
    for row in values[1:]:
        count += 1
        if not row:
            break
        try:
            if not str(row[1+column]) == "":
                found = str(row[1+column])
                relevantLuck.append(found)
                if search:
                    distance = difflib.SequenceMatcher(
                        None, search.lower(), found[:len(search)].lower()).ratio()
                    if distance > 0.85:
                        return found
        except:
            pass

    if not search:
        index = random.randint(0, len(relevantLuck)-1)
    if index:
        return str(relevantLuck[index])


async def skill(skill, arg):
    sheet = service.spreadsheets()
    index = None
    result = sheet.values().get(spreadsheetId=SuggestionID,
                                range='Skills!A2:R100').execute()
    values = result.get('values', [])
    category = skill
    skill = skill.lower()
    arg = arg.lower()
    if skill in ["list", "brawn", "athletics", "dexterity", "social", "wits", "knowledge", "guts"]:
        # We are listing skills!
        statBreakdown = {
            "brawn": [],
            "athletics": [],
            "dexterity": [],
            "social": [],
            "wits": [],
            "knowledge": [],
            "guts": []
        }
        for row in values:
            for key in statBreakdown.keys():
                if key == str(row[1]).lower():
                    statBreakdown[key] += [str(row[0])]
                if key == str(row[2]).lower():
                    statBreakdown[key] += [str(row[0])]

        if skill in ["brawn", "athletics", "dexterity", "social", "wits", "knowledge", "guts"]:
            string = category + ":\n"
            listing = ""
            for i in statBreakdown[skill]:
                listing += i + ", "
            string += listing[:-2]
            return string
        else:
            string = ""
            for category in ["Brawn", "Athletics", "Dexterity", "Social", "Wits", "Knowledge", "Guts"]:
                string += category + ":\n"
                listing = ""
                for i in statBreakdown[category.lower()]:
                    listing += i + ", "
                string += listing[:-2] + "\n\n"
            return string

    for row in values:
        if skill == str(row[0]).lower():
            # Always start with name and categories
            string = str(row[0]) + "(" + str(row[1])
            if str(row[2]) != "-":
                string += ", " + str(row[2])
            string += ")\n"
            if arg == "basic":
                # Print basic stuff
                string += str(row[3])
                if str(row[16]) == "Y":
                    string += "\n"
                    string += "There is additional information for this skill "
                    string += "that is not stored here, check the Misc doc."
            elif arg == "short":
                string += str(row[4])
            elif arg == "1":
                string += "Pip ●: "
                string += str(row[5])
            elif arg == "2":
                string += "Pip ●●: "
                string += str(row[6])
            elif arg == "3":
                string += "Pip ●●●: "
                string += str(row[7])
            elif arg == "4":
                string += "Pip ●●●●: "
                string += str(row[8])
            elif arg == "5":
                string += "Pip ●●●●●: "
                string += str(row[9])
            elif arg == "specialities" or arg == "speciality":
                special = False
                for x in range(0, 6):
                    if str(row[10+x]) != "-":
                        if not special:
                            special = True
                            string += "Specialities:"
                        string += "\n" + str(row[10+x])
            else:
                string = "Do not recognise arguement " + str(arg)
            return string

    return "Haven't added this skill yet"


async def trading_capes():
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TradingDatabaseID,
                                range='Capes!A2:K1000').execute()
    values = result.get('values', [])
    allinfo = []
    for row in values:
        try:
            if not str(row[0]) == "":
                allinfo += [row]
            else:
                break
        except:
            break

    return allinfo


async def owned_cards():
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TradingDatabaseID,
                                range='Owned!A2:B30000').execute()
    values = result.get('values', [])
    allinfo = []
    for row in values:
        try:
            if not str(row[0]) == "":
                allinfo += [row]
            else:
                break
        except:
            break

    return allinfo


async def gain_cards(claimer, cards):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TradingDatabaseID,
                                range='Owned!A2:B30000').execute()
    values = result.get('values', [])
    used_rows = 1
    for row in values:
        try:
            if not str(row[0]) == "":
                used_rows += 1
            else:
                break
        except:
            break

    data_to_paste = "<table>"
    for i in cards:
        data_to_paste += "<tr><td>{}</td>".format(claimer)
        data_to_paste += "<td>{}</td></tr>".format(str(i))
    data_to_paste += "</table>"

    paste_req = {
        "pasteData": {
            "coordinate": {
                "sheetId": 1429634698,
                "rowIndex": used_rows,
                "columnIndex": 0
            },
            "data": data_to_paste,
            "type": "PASTE_VALUES",
            "html": True
        }
    }
    reqs = [paste_req]
    batch_res = sheet.batchUpdate(spreadsheetId=TradingDatabaseID, body={
                                  "requests": reqs}).execute()


async def move_card_owner(old, new, cards):
    if len(cards) == 0:
        return
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TradingDatabaseID,
                                range='Owned!A2:B30000').execute()
    values = result.get('values', [])
    reqs = []
    for card in cards:
        for row in range(0, len(values)):
            try:
                owner = str(values[row][0])
                c_index = str(values[row][1])
                if owner == old and c_index == str(card):
                    values[row][0] = new
                    reqs += [{
                        "pasteData": {
                            "coordinate": {
                                "sheetId": 1429634698,
                                "rowIndex": row+1,
                                "columnIndex": 0
                            },
                            "data": '<table><tr><td>' + new + '</td></tr></table>',
                            "type": "PASTE_VALUES",
                            "html": True
                        }
                    }]
                    break
            except:
                continue

    batch_res = sheet.batchUpdate(spreadsheetId=TradingDatabaseID, body={
                                  "requests": reqs}).execute()


async def remove_cards(owner, cards):
    if len(cards) == 0:
        return
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TradingDatabaseID,
                                range='Owned!A2:B30000').execute()
    values = result.get('values', [])
    rows_to_delete = []
    for card in cards:
        for row in range(0, len(values)):
            try:
                own = str(values[row][0])
                c_index = str(values[row][1])
                if owner == own and c_index == str(card) and not row in rows_to_delete:
                    rows_to_delete += [row]
                    break
            except:
                continue

    if len(rows_to_delete) != len(cards):
        return

    # Delete in reverse order!
    rows_to_delete.sort(reverse=True)
    for i in rows_to_delete:
        del_req = {
            "deleteDimension": {
                "range": {
                    "sheetId": 1429634698,  # Triggers
                    "dimension": "ROWS",
                    "startIndex": i+1,
                    "endIndex": i+2
                }
            }
        }
        batch_res = sheet.batchUpdate(spreadsheetId=TradingDatabaseID, body={
                                      "requests": [del_req]}).execute()


async def get_battle_stats():
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TradingDatabaseID,
                                range='Battle Stats!A2:G20').execute()
    values = result.get('values', [])
    stats = {"classes": {}, "multipliers": {}}
    for i in values[:12]:
        stats["classes"][i[0]] = {}
        stats["classes"][i[0]]["Defend"] = float(i[1])
        stats["classes"][i[0]]["Attack"] = float(i[2])
        stats["classes"][i[0]]["Flee"] = float(i[3])
        stats["classes"][i[0]]["Ambush"] = float(i[4])
        stats["classes"][i[0]]["Health"] = float(i[5])
        stats["classes"][i[0]]["Wits"] = float(i[6])

    for i in values[14:]:
        stats["multipliers"][i[0]] = {}
        stats["multipliers"][i[0]]["Defend"] = float(i[1])
        stats["multipliers"][i[0]]["Attack"] = float(i[2])
        stats["multipliers"][i[0]]["Wits"] = float(i[3])

    return stats


async def waiting_triggers():
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerSuggestionsID,
                                range='Trigger Suggestions!A2:C1000').execute()
    values = result.get('values', [])
    triggers = {}
    for row in values:
        try:
            if str(row[0]) != "":
                trigger = str(row[0])
                reviewers = []
                try:
                    # Don't let people review their own trigger
                    r_string = str(row[1])
                    r_string = r_string.split("(")[-1]
                    r_string = r_string.split(")")[0]
                    if r_string.strip() != "":
                        reviewers += [r_string.strip()]
                except:
                    pass
                try:
                    r_string = str(row[2])
                    r_string = r_string.split(",")
                    for i in r_string:
                        if i != "":
                            reviewers += [i.strip()]
                except:
                    pass
                triggers[trigger] = reviewers
            else:
                break
        except:
            continue
    return triggers


async def get_relevant_trigger_row(triggers, trigger):
    for row in range(0, len(triggers)):
        try:
            if str(triggers[row][0]) != "":
                if str(triggers[row][0]) == trigger:
                    return row
            else:
                break
        except:
            continue


async def submit_trigger(trigger, u_id, u_name):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerSuggestionsID,
                                range='Trigger Suggestions!A2:F1000').execute()
    triggers = result.get('values', [])
    index = 0
    for row in triggers:
        try:
            if str(row[0]) == "":
                break
            else:
                index += 1
        except:
            break  # Assuming that failure also means empty row!

    data_to_paste = '<table><tr><td>' + trigger + \
        '</td><td>' + u_name + ' (' + str(u_id) + ')</tr></table>'

    paste_req = {
        "pasteData": {
            "coordinate": {
                "sheetId": 1029421998,
                "rowIndex": index+1,
                "columnIndex": 0
            },
            "data": data_to_paste,
            "type": "PASTE_VALUES",
            "html": True
        }
    }
    reqs = [paste_req]
    batch_res = sheet.batchUpdate(spreadsheetId=TriggerSuggestionsID, body={
                                  "requests": reqs}).execute()


async def approve_trigger(trigger, u_id, u_name, comments=""):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerSuggestionsID,
                                range='Trigger Suggestions!A2:F1000').execute()
    triggers = result.get('values', [])
    index = await get_relevant_trigger_row(triggers, trigger)
    ids = ""
    users = ""
    comm = ""
    try:
        if str(triggers[index][2]) != "":
            ids = str(triggers[index][2]) + ", "
    except:
        pass
    try:
        if str(triggers[index][3]) != "":
            users = str(triggers[index][3]) + ", "
    except:
        pass
    try:
        comm = str(triggers[index][4])
        if comments != "" and comm != "":
            comm += ", "
    except:
        pass
    ids += str(u_id)
    users += str(u_name)
    if comments != "":
        comm += comments + " - " + str(u_name)

    data_to_paste = '<table><tr><td>' + ids + \
        '</td><td>' + users + \
        '</td><td>' + comm + '</tr></table>'

    paste_req = {
        "pasteData": {
            "coordinate": {
                "sheetId": 1029421998,
                "rowIndex": index+1,
                "columnIndex": 2
            },
            "data": data_to_paste,
            "type": "PASTE_VALUES",
            "html": True
        }
    }
    reqs = [paste_req]
    batch_res = sheet.batchUpdate(spreadsheetId=TriggerSuggestionsID, body={
                                  "requests": reqs}).execute()


async def critique_trigger(trigger, u_id, u_name, comments):
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=TriggerSuggestionsID,
                                range='Trigger Suggestions!A2:F1000').execute()
    triggers = result.get('values', [])
    index = await get_relevant_trigger_row(triggers, trigger)
    ids = ""
    users = ""
    comm = ""
    try:
        if str(triggers[index][2]) != "":
            ids = str(triggers[index][2]) + ", "
    except:
        pass
    try:
        users = str(triggers[index][3])
    except:
        pass
    try:
        comm = str(triggers[index][4])
        if comments != "" and comm != "":
            comm += ", "
    except:
        pass
    ids += str(u_id)
    if comments != "":
        comm += comments + " - " + str(u_name)

    data_to_paste = '<table><tr><td>' + ids + \
        '</td><td>' + users + \
        '</td><td>' + comm + '</tr></table>'

    paste_req = {
        "pasteData": {
            "coordinate": {
                "sheetId": 1029421998,
                "rowIndex": index+1,
                "columnIndex": 2
            },
            "data": data_to_paste,
            "type": "PASTE_VALUES",
            "html": True
        }
    }
    reqs = [paste_req]
    batch_res = sheet.batchUpdate(spreadsheetId=TriggerSuggestionsID, body={
                                  "requests": reqs}).execute()


async def cape(name):
    sheet = service.spreadsheets()
    index = None
    result = sheet.values().get(spreadsheetId=CapesDatabaseID,
                                range='Non-Canon!A2:K1833').execute()
    values = result.get('values', [])
    count = 0

    for row in values:
        try:
            if not str(row[0]) == "":
                if str(row[0]) == name:
                    return row
                count += 1
        except:
            pass

    if name == "":
        num = random.randrange(0, count)
        return values[num]

    return None


async def documents():
    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Documents!A2:C1000').execute()
    values = result.get('values', [])

    data = {}
    for row in values:
        try:
            if not str(row[0]) == "":
                data[str(row[0])] = str(row[1])
        except:
            pass

    return data


async def add_document(doc, link, submitter):
    sheet = service.spreadsheets()
    index = 1
    result = sheet.values().get(spreadsheetId=ID1,
                                range='Documents!A2:C1000').execute()
    values = result.get('values', [])
    for row in values:
        try:
            if not str(row[0]) == "":
                index += 1
            else:
                break
        except:
            break

    data_to_paste = '<table><tr><td>' + doc + \
        '</td><td>' + link + \
        '</td><td>' + submitter + '</td></tr></table>'

    paste_req = {
        "pasteData": {
            "coordinate": {
                "sheetId": 2032145493,
                "rowIndex": index,
                "columnIndex": 0
            },
            "data": data_to_paste,
            "type": "PASTE_VALUES",
            "html": True
        }
    }
    reqs = [paste_req]
    batch_res = sheet.batchUpdate(spreadsheetId=ID1, body={
                                  "requests": reqs}).execute()


# ...sorry about the mess X|
# I'm back, more mess!


supreme = {
    "userEnteredFormat":
    {
        "backgroundColor": {"red": 0.945, "blue": 0.196, "green": 0.761},
        "textFormat": {"foregroundColor": {"red": 0, "blue": 0, "green": 0}}
    },
    "userEnteredValue": {"stringValue": "S"}
}
good = {
    "userEnteredFormat":
        {
            "backgroundColor": {"red": 0.714, "blue": 0.659, "green": 0.843},
            "textFormat": {"foregroundColor": {"red": 0, "blue": 0, "green": 0}}
        },
        "userEnteredValue": {"stringValue": "G"}
}
moderate = {
    "userEnteredFormat":
    {
        "backgroundColor": {"red": 0.643, "blue": 0.957, "green": 0.761},
        "textFormat": {"foregroundColor": {"red": 0, "blue": 0, "green": 0}}
    },
    "userEnteredValue": {"stringValue": "M"}
}
poor = {
    "userEnteredFormat":
        {
            "backgroundColor": {"red": 0.706, "blue": 0.839, "green": 0.655},
            "textFormat": {"foregroundColor": {"red": 0, "blue": 0, "green": 0}}
        },
        "userEnteredValue": {"stringValue": "P"}
}
awful = {
    "userEnteredFormat":
    {
        "backgroundColor": {"red": 0.957, "blue": 0.8, "green": 0.8},
        "textFormat": {"foregroundColor": {"red": 0, "blue": 0, "green": 0}}
    },
    "userEnteredValue": {"stringValue": "A"}
}
worst = {
    "userEnteredFormat":
    {
        "backgroundColor": {"red": 0, "blue": 0, "green": 0},
        "textFormat": {"foregroundColor": {"red": 1, "blue": 1, "green": 1}}
    },
    "userEnteredValue": {"stringValue": "W"}
}


async def new_alt_sheet():

    # Create a doc
    title = "Pact Dice Draft - " + \
        datetime.datetime.now(datetime.timezone.utc).isoformat()
    spreadsheet = {"properties": {"title": title}}
    drive_response = drive_service.files().copy(
        fileId='1MVI5EogPTq20OR-br9suw-PgnyZTno8KyNQA03gkyE8', body=spreadsheet).execute()
    ID = drive_response.get('id')
    requests = [{"updateSpreadsheetProperties": {
        "properties": {"title": title}, "fields": "title"}}]
    result = service.spreadsheets().batchUpdate(
        spreadsheetId=ID, body={"requests": requests}).execute()
    view = {"type": "anyone", "role": "reader"}
    perm_result = drive_service.permissions().create(fileId=ID, body=view).execute()

    p = [[], [], [], [], []]
    a = [[], [], [], [], []]
    l = [[], [], [], [], []]
    e = [[], [], [], [], []]
    s = [[], [], [], [], []]
    f = [[], [], [], [], []]
    stats = [p, a, l, e, s, f]
    pool = [5, 5, 5, 4, 4, 4, 4, 4, 3, 3, 3, 3,
            3, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 0, 0]

    for stat in stats:
        random.shuffle(pool)
        marker = 0
        for row in stat:
            row.append(pool[marker])
            marker += 1
            while marker % 5 != 0:
                row.append(pool[marker])
                marker += 1

        if stat[2][2] != 3:
            val = stat[2][2]
            r_count = 0
            for row in stat:
                c_count = 0
                for col in row:
                    if col == 3:
                        stat[r_count][c_count] = val
                        stat[2][2] = 3
                        break
                    c_count += 1
                if stat[2][2] == 3:
                    break
                r_count += 1
    requests = []

    def distribute(row_num, col_num, array):
        r = row_num
        for row in array:
            c = col_num
            for col in row:
                cell = {}
                if col == 0:
                    cell = worst
                elif col == 1:
                    cell = awful
                elif col == 2:
                    cell = poor
                elif col == 3:
                    cell = moderate
                elif col == 4:
                    cell = good
                elif col == 5:
                    cell = supreme
                ran = {"sheetId": 0, "startRowIndex": r, "endRowIndex": r +
                       1, "startColumnIndex": c, "endColumnIndex": c + 1}
                repeat_cell = {"range": ran, "cell": cell,
                               "fields": "userEnteredFormat.backgroundColor,userEnteredValue.stringValue,userEnteredFormat.textFormat.foregroundColor"}
                requests.append({"repeatCell": repeat_cell})
                c += 1
            r += 1

    distribute(2, 2, p)
    distribute(2, 9, a)
    distribute(2, 16, l)
    distribute(9, 2, e)
    distribute(9, 9, s)
    distribute(9, 16, f)

    result = service.spreadsheets().batchUpdate(
        spreadsheetId=ID, body={"requests": requests}).execute()

    return ID


async def read_sheet(id, rs):
    return service.spreadsheets().values().batchGet(spreadsheetId=id, ranges=rs).execute()


async def get_sheet_colors(id):
    raw = service.spreadsheets().get(spreadsheetId=id, ranges=[
        'C3:G7', 'J3:N7', 'Q3:U7', 'C10:G14', 'J10:N14', 'Q10:U14'], includeGridData=True).execute()
    colors = []
    for d in raw['sheets'][0]['data']:
        for r in d['rowData']:
            for v in r['values']:
                colors.append(v['userEnteredFormat']['backgroundColor'])
    return colors


async def update_sheet(id, requests):
    service.spreadsheets().batchUpdate(
        spreadsheetId=id, body={"requests": requests}).execute()


def translate(cat, cell):
    out = (0, 0)
    cell = cell.lower()
    col = ord(cell[0]) - 97
    row = int(cell[1]) - 1
    if cat.lower() == 'p':
        out = (col, row)
    elif cat.lower() == 'a':
        out = (col + 5, row)
    elif cat.lower() == 'l':
        out = (col + 10, row)
    elif cat.lower() == 'e':
        out = (col, row + 5)
    elif cat.lower() == 's':
        out = (col + 5, row + 5)
    elif cat.lower() == 'f':
        out = (col + 10, row + 5)
    return out


async def transfer_hand(cells, pBox, ID, grays, deck, name, ctx):
    counter = 0
    requests = []
    weights = [0, 0, 0, 0, 0, 0]
    cat = cells.pop(0)

    draws = 0
    for cell in cells:
        if cell.lower() == 'draw' or (cat, cell) in grays:
            draws += 1
    removes = max(0, len(cells) - 5)
    spares = []
    if draws > 0:
        spares, weights = await draw(deck, draws, removes, name, ctx)
    while len(cells) > 5:
        cells.pop()
    for cell in cells:
        # Where do we need to put things?
        prow = 29 + (8 * int(pBox / 3)) + counter
        pcol = ((7 * (pBox % 3)) + 1)
        if cat == None:
            pcol += 1 + int(cells[0][0]/5) + (3 * int(cells[0][1]/5))
        elif cat == 'p':
            pcol += 1
        elif cat == 'a':
            pcol += 2
        elif cat == 'l':
            pcol += 3
        elif cat == 'e':
            pcol += 4
        elif cat == 's':
            pcol += 5
        elif cat == 'f':
            pcol += 6
        player_box_range = {"sheetId": 0, "startRowIndex": prow, "endRowIndex": prow + 1, "startColumnIndex": pcol,
                            "endColumnIndex": pcol + 1}

        if cell.lower() != 'draw' and (cat, cell) not in grays:
            # If we're grabbing the cell from the grid,
            # where do we need to grab things?
            c = translate(cat, cell)
            srow = c[1] + (2 * (int(c[1] / 5) + 1))
            scol = c[0] + (2 * (int(c[0] / 5) + 1))
            origin_range = {"sheetId": 0, "startRowIndex": srow, "endRowIndex": srow + 1, "startColumnIndex": scol,
                            "endColumnIndex": scol + 1}

            # Copy the cell to the player box
            copy_paste = {"source": origin_range,
                          "destination": player_box_range,
                          "pasteType": "PASTE_NO_BORDERS",
                          "pasteOrientation": "NORMAL"}
            requests.append({"copyPaste": copy_paste})

            # And replace it with a gray cell
            grayCell = {"userEnteredFormat": {"backgroundColor": {"red": 0.5, "blue": 0.5, "green": 0.5}},
                        "userEnteredValue": {"stringValue": ""}}
            repeat_cell = {"range": origin_range, "cell": grayCell,
                           "fields": "userEnteredFormat.backgroundColor,userEnteredValue.stringValue"}
            requests.append({"repeatCell": repeat_cell})
            grays.add((cat, cell))
        else:
            # If we're drawing, put the drawn card in instead
            cellValue = spares.pop()
            repeat_cell = {"range": player_box_range, "cell": cellValue,
                           "fields": "userEnteredFormat.backgroundColor,userEnteredFormat.textFormat.foregroundColor,userEnteredValue.stringValue"}
            requests.append({"repeatCell": repeat_cell})

        counter += 1

    result = service.spreadsheets().batchUpdate(
        spreadsheetId=ID, body={"requests": requests}).execute()

    return weights


async def get_deck(id):
    odds = [int(y[0]) for y in service.spreadsheets().values().get(
        spreadsheetId=id, range='D20:D25').execute()['values']]
    output = []
    for x in range(int(odds[0])):
        output.append(supreme)
    for x in range(int(odds[1])):
        output.append(good)
    for x in range(int(odds[2])):
        output.append(moderate)
    for x in range(int(odds[3])):
        output.append(poor)
    for x in range(int(odds[4])):
        output.append(awful)
    for x in range(int(odds[5])):
        output.append(worst)
    return output


async def adjust_deck(id, vals):
    og = [int(y[0]) for y in service.spreadsheets().values().get(
        spreadsheetId=id, range='D20:D25').execute()['values']]
    for x in range(6):
        og[x] += vals[x]
    if all(x <= 0 for x in og):
        og[0] += 1
        og[1] += 2
        og[2] += 3
        og[3] += 4
        og[4] += 5
        og[5] += 5
    r = []
    count = 0
    for val in og:
        r.append({"repeatCell": {"range": {"sheetId": 0, "startRowIndex": 19 + count, "endRowIndex": 19 + count + 1,
                                           "startColumnIndex": 3, "endColumnIndex": 4},
                                 "cell": {"userEnteredValue": {"numberValue": val}},
                                 "fields": "userEnteredValue(stringValue)"}})
        count += 1
    await update_sheet(id, r)


async def draw(deck, amount, removal, name, ctx):
    og_deck = [0, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5]
    cards = [supreme, good, moderate, poor, awful, worst]
    ordered_deck = []
    for card in deck:
        ordered_deck.append(cards.index(card))
    diff = 0
    if len(ordered_deck) < amount:
        diff = amount - len(ordered_deck)
        amount = len(ordered_deck)
    selection = random.sample(ordered_deck, amount) + \
        random.sample(og_deck, diff)
    selection.sort()
    msg = name + " drew: "
    count = 0
    for s in selection:
        count += 1
        if count == 6:
            msg += "~~"
        if s == 0:
            msg += "Supreme, "
        elif s == 1:
            msg += "Good, "
        elif s == 2:
            msg += "Moderate, "
        elif s == 3:
            msg += "Poor, "
        elif s == 4:
            msg += "Awful, "
        elif s == 5:
            msg += "Worst, "
    msg = msg[:-2]
    if len(selection) > 5:
        msg += "~~"
    await ctx.send(msg)
    output = []
    weights = [0, 0, 0, 0, 0, 0]
    for num in selection:
        output.append(cards[num])
        weights[num] -= 1
    for x in range(removal):
        output.pop()
    return output, weights


async def add_black_mark(id, cat, cell):
    entry = translate(cat, cell)
    row = entry[1]
    col = entry[0]
    if entry[1] > 4:
        row = entry[1] + 4
    elif entry[1] >= 0:
        row = entry[1] + 2
    if entry[0] > 9:
        col = entry[0] + 6
    elif entry[0] > 4:
        col = entry[0] + 4
    elif entry[0] >= 0:
        col = entry[0] + 2
    txt = "★"
    o = await read_sheet(id, [(str(chr(ord('@')+col+1)) + str(row+1))])
    if "values" in o['valueRanges'][0]:
        txt = o['valueRanges'][0]['values'][0][0] + '★'
    r = {"repeatCell": {"range": {"sheetId": 0, "startRowIndex": row, "endRowIndex": row+1,
                                  "startColumnIndex": col, "endColumnIndex": col+1},
                        "cell": {"userEnteredValue": {"stringValue": txt}},
                        "fields": "userEnteredValue.stringValue"}}
    await update_sheet(id, r)


async def add_white_mark(id, pBox, cat):
    which = random.randint(0, 4)
    row = 30 + (8 * int(pBox / 3)) + which
    col = ((7 * (pBox % 3)) + 1)
    if cat == 'p':
        col += 1
    elif cat == 'a':
        col += 2
    elif cat == 'l':
        col += 3
    elif cat == 'e':
        col += 4
    elif cat == 's':
        col += 5
    elif cat == 'f':
        col += 6

    txt = "☆"
    o = await read_sheet(id, [(str(chr(ord('@') + col + 1)) + str(row + 1))])
    if "values" in o['valueRanges'][0]:
        txt = o['valueRanges'][0]['values'][0][0] + '☆'
    r = {"repeatCell": {"range": {"sheetId": 0, "startRowIndex": row, "endRowIndex": row + 1,
                                  "startColumnIndex": col, "endColumnIndex": col + 1},
                        "cell": {"userEnteredValue": {"stringValue": txt}},
                        "fields": "userEnteredValue.stringValue"}}
    await update_sheet(id, r)


async def send_black_mark(id, pBox, cat):
    which = random.randint(0, 4)
    row = 30 + (8 * int(pBox / 3)) + which
    col = ((7 * (pBox % 3)) + 1)
    if cat == 'p':
        col += 1
    elif cat == 'a':
        col += 2
    elif cat == 'l':
        col += 3
    elif cat == 'e':
        col += 4
    elif cat == 's':
        col += 5
    elif cat == 'f':
        col += 6

    txt = "★"
    o = await read_sheet(id, [(str(chr(ord('@') + col + 1)) + str(row + 1))])
    if "values" in o['valueRanges'][0]:
        txt = o['valueRanges'][0]['values'][0][0] + '★'
    r = {"repeatCell": {"range": {"sheetId": 0, "startRowIndex": row, "endRowIndex": row + 1,
                                  "startColumnIndex": col, "endColumnIndex": col + 1},
                        "cell": {"userEnteredValue": {"stringValue": txt}},
                        "fields": "userEnteredValue(stringValue)"}}
    await update_sheet(id, r)
